#include <Adafruit_Crickit.h>
#include <seesaw_servo.h>
#include <seesaw_neopixel.h>

#define NEOPIN 20
 
static Adafruit_Crickit crickit;
static seesaw_Servo myservo(&crickit);  // create servo object to control a servo
// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
static seesaw_NeoPixel strip = seesaw_NeoPixel(12, NEOPIN, NEO_GRB + NEO_KHZ800);
static unsigned long  next;

static void
colorWipe(uint32_t c) {
  for (uint16_t i=0; i<strip.numPixels(); ++i) {
    strip.setPixelColor(i, c);
    strip.show();
  }
}

void
setup()
{
  
  if (!crickit.begin()) {
    while(1);
  }

  if (!strip.begin()) {
    while(1);
  }

  pinMode(LED_BUILTIN, OUTPUT);

  myservo.attach(CRICKIT_SERVO1);  // attaches the servo to CRICKIT_SERVO1 pin
  strip.show();
  next = 0;
}
 
void
loop()
{
  colorWipe(strip.Color(255, 0, 0));
  myservo.write(0);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  colorWipe(strip.Color(0, 0, 255));
  myservo.write(90);
  delay(1000);
  colorWipe(strip.Color(255, 0, 255));
  myservo.write(180);
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
  colorWipe(strip.Color(0, 255, 0));
  myservo.write(90);
  delay(1000);
}
